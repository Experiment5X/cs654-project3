# RSA Cracker

This application will find values to crack RSA if the exponent is 3. The value and modulus are supplied by the user as command line arguments.

```
Usage: BreakingRSA value modulus
```

## Running the Application
To run the sequential version run the following commands:

```
make seq
./bin/BreakingRSA-Seq 2 1033
```

To run the parallel version run the following commands:
```
make 
./bin/BreakingRSA 2 1033
```

## Timing Results

#### Parallel
```
$ time ./bin/BreakingRSA 46054145 124822069
142857^3 = 46054145 (mod 124822069)
27549958^3 = 46054145 (mod 124822069)
97129254^3 = 46054145 (mod 124822069)

real	0m1.105s
user	0m0.008s
sys	    0m0.262s
```

#### Seqential
```
$ time ./bin/BreakingRSA-Seq 46054145 124822069
142857^3 = 46054145 (mod 124822069)
27549958^3 = 46054145 (mod 124822069)
97129254^3 = 46054145 (mod 124822069)

real	0m5.927s
user	0m2.448s
sys	    0m0.000s
```