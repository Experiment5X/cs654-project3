
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/**
 * Find all of the numbers such that: n^3 = value mod (modulus)
 * 
 * @param modulus The modulus in the equation
 * @param value The value to be the remainder
 * @param result The start of the array to put results
 * @param endResult The end of the array of results. If more results are found
 *                  than there are spots in the array, those values will be
 *         		    truncated.
 */
__global__ void rsaCrackKernel(unsigned long long modulous, unsigned long long value, 
	unsigned long long *result, unsigned long long *endResult)
{
	unsigned int globalThreadIndex = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int totalThreadCount = gridDim.x * blockDim.x;

	// determine which guesses this thread is responsible for
	unsigned long long guessStart = (modulous * globalThreadIndex) / totalThreadCount;
	unsigned long long guessEnd = (modulous * (globalThreadIndex + 1)) / totalThreadCount;

	// go through all the guesses that this thread is responsible for
	for (unsigned long long guessCbrt = guessStart; guessCbrt < guessEnd; guessCbrt++) 
	{
		// calculate: guessCbrt^3 mod modulous
		unsigned long long attempt = (((guessCbrt * guessCbrt) % modulous) * guessCbrt) % modulous;
		if (attempt == value)
		{
			// 1. Put the new value into the global buffer at the next spot I think is free (result)
			// 2. Pull out the old value
			// 3. If it isn't 0, that means we have to put it back in the buffer at the next spot
			//    I think is empty (result + 1)
			unsigned long long toInsert = guessCbrt;
			while (result < endResult && toInsert != 0)
			{
				toInsert = atomicExch(result, toInsert);
				result++;
			}
		}
	}
}

/*
 * Counts the number of elements in a zero terminated list of positive integers
 *
 * @param start The beginning of the array
 * @param end The end of the array
 * @return The number of elements in the array
 */
int countElements(unsigned long long *start, unsigned long long *end) 
{
	int length = 0;
	unsigned long long* iter = start;
	while (iter < end && *iter != 0)
	{
		length++;
		iter++;
	}

	return length;
}

/*
 * Compare two unsigned long longs for the qsort() method
 *
 * @param a Pointer to an unsigned long long
 * @param b Pointer to an unsigned long long
 * @return The difference between these two numbers (a-b)
 */
int compareULL(const void *a, const void *b)
{
	unsigned long long *numA = (unsigned long long*)a;
	unsigned long long *numB = (unsigned long long*)b;

	return *numA - *numB;
}

int main(int argc, char *argv[])
{
	unsigned long long modulous = 124822069;
	unsigned long long value = 46054145;

	if (argc != 3) 
	{
		printf("Usage: BreakingRSA value modulous\r\n");
	} 
	else
	{
		sscanf(argv[1], "%llu", &value);
		sscanf(argv[2], "%llu", &modulous);
	}

	size_t bufferEntryCount = 100;
	size_t bufferSize = sizeof(unsigned long long) * bufferEntryCount;
	
	// allocate device memory and zero it
	unsigned long long *d_result;
	cudaMalloc(&d_result, bufferSize);
	cudaMemset(d_result, 0, bufferSize);
	
	rsaCrackKernel<<<256, 1024>>>(modulous, value, d_result, d_result + bufferEntryCount);

	// allocate host memory
	unsigned long long *result = (unsigned long long*)malloc(bufferSize);
	unsigned long long* resultEnd = result + bufferEntryCount;

	// copy over the device result data to the host memory
	cudaMemcpy(result, d_result, bufferSize, cudaMemcpyDeviceToHost);

	// sort the results with lowest values first
	int resultCount = countElements(result, resultEnd);
	qsort(result, resultCount, sizeof(unsigned long long), compareULL);

	// print out all the result
	unsigned long long* resultIter = result;
	while (resultIter < resultEnd && *resultIter != 0) 
	{
		printf("%llu^3 = %llu (mod %llu)\r\n", *resultIter, value, modulous);
		resultIter++;
	}
	if (*result == 0) 
	{
		printf("No cube roots of %llu (mod %llu)\r\n", value, modulous);
	}

	free(result);
	cudaFree(d_result);

	return 0;
}
