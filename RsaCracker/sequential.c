#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Find all of the numbers such that: n^3 = value mod (modulus)
 * 
 * @param modulus The modulus in the equation
 * @param value The value to be the remainder
 * @param result The start of the array to put results
 * @param endResult The end of the array of results. If more results are found
 *                  than there are spots in the array, those values will be
 *         		    truncated.
 */
void rsaCrackKernel(unsigned long long modulus, unsigned long long value, 
	unsigned long long *result, unsigned long long *endResult)
{
	// go through all the possible values
	for (unsigned long long guessCbrt = 0; guessCbrt < modulus; guessCbrt++) 
	{
		// calculate: guessCbrt^3 mod modulus
		unsigned long long attempt = (((guessCbrt * guessCbrt) % modulus) * guessCbrt) % modulus;
		if (attempt == value)
		{
            *result = guessCbrt;
            result++;
		}
	}
}

int main(int argc, char *argv[]) 
{
	unsigned long long modulus = 124822069;
	unsigned long long value = 46054145;

	// parse the command line arguments
	if (argc != 3) 
	{
		printf("Usage: BreakingRSA value modulus\r\n");
	} 
	else
	{
		sscanf(argv[1], "%llu", &value);
		sscanf(argv[2], "%llu", &modulus);
	}

	// allocate a buffer for the results
    static const size_t bufferLength = 100;
    unsigned long long *results = malloc(sizeof(unsigned long long) * bufferLength);
    unsigned long long *resultsEnd = results + bufferLength;

    memset(results, 0, sizeof(unsigned long long) * bufferLength);
    rsaCrackKernel(modulus, value, results, results + bufferLength);

	// display the results
    if (*results == 0)
    {
		printf("No cube roots of %llu (mod %llu)\r\n", value, modulus);
    }
    else
    {
        unsigned long long *resultIter = results;
        while (resultIter < resultsEnd && *resultIter != 0) 
        {
            printf("%llu^3 = %llu (mod %llu)\r\n", *resultIter, value, modulus);
            resultIter++;
        }
    }

    free(results);
}