all:
	@mkdir -p ./bin/
	@nvcc -o ./bin/BreakingRSA ./RsaCracker/kernel.cu

seq:
	@mkdir -p ./bin/
	@gcc -o ./bin/BreakingRSA-Seq ./RsaCracker/sequential.c

clean:
	@rm -rf ./bin/
